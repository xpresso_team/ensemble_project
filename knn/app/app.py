import pickle
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split


df = pd.read_csv('data/diabetes_data.csv')

print(df.head())

print(df.shape)

X = df.drop(columns=['diabetes'])
y = df['diabetes']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, stratify=y)

# create new a knn model
knn = KNeighborsClassifier()
# create a dictionary of all values we want to test for n_neighbors
params_knn = {'n_neighbors': np.arange(1, 25)}
# use gridsearch to test all values for n_neighbors
knn_gs = GridSearchCV(knn, params_knn, cv=5)
# fit model to training data
knn_gs.fit(X_train, y_train)

knn_best = knn_gs.best_estimator_
print(f'KNN Score: {knn_best.score(X_test, y_test)}')

# save the model to disk
filename = '/model/knn.sav'
pickle.dump(knn_best, open(filename, 'wb'))

print('KNN model saved!')
