import pickle
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split


df = pd.read_csv('data/diabetes_data.csv')

print(df.head())

print(df.shape)

X = df.drop(columns=['diabetes'])
y = df['diabetes']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, stratify=y)

# create a new logistic regression model
log_reg = LogisticRegression()

# fit the model to the training data
log_reg.fit(X_train, y_train)
print(f'LR Score: {log_reg.score(X_test, y_test)}')

# save the model to disk
filename = '/model/lr.sav'
pickle.dump(log_reg, open(filename, 'wb'))

print('LR model saved!')