import pickle
from sklearn.ensemble import VotingClassifier
import pandas as pd
from sklearn.model_selection import train_test_split


df = pd.read_csv('data/diabetes_data.csv')

X = df.drop(columns=['diabetes'])
y = df['diabetes']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, stratify=y)


knn_path = '/model/knn.sav'
lr_path = '/model/lr.sav'
rf_path = '/model/rf_best.sav'

knn_best = pickle.load(open(knn_path, 'rb'))
rf_best = pickle.load(open(rf_path, 'rb'))
log_reg = pickle.load(open(lr_path, 'rb'))

estimators = [('knn', knn_best), ('rf', rf_best), ('log_reg', log_reg)]

# create our voting classifier, inputting our models
ensemble = VotingClassifier(estimators, voting='hard')

# fit model to training data
ensemble.fit(X_train, y_train)

# test our model on the test data
print(f'Ensemble score : {ensemble.score(X_test, y_test)}')

