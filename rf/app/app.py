import pickle
import pandas as pd
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split


df = pd.read_csv('data/diabetes_data.csv')

print(df.head())

print(df.shape)

X = df.drop(columns=['diabetes'])
y = df['diabetes']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, stratify=y)

rf = RandomForestClassifier()

# create a dictionary of all values we want to test for n_estimators
params_rf = {'n_estimators': [50, 100, 200]}

# use gridsearch to test all values for n_estimators
rf_gs = GridSearchCV(rf, params_rf, cv=5)

# fit model to training data
rf_gs.fit(X_train, y_train)

# save best model
rf_best = rf_gs.best_estimator_
print(f'RF Score: {rf_best.score(X_test, y_test)}')

filename = '/model/rf_best.sav'
pickle.dump(rf_best, open(filename, 'wb'))
print('RF model saved!')
